import 'dart:convert';

import 'package:dev_test/package.dart';
import 'package:process_run/shell.dart';

Future main() async {
  print(JsonEncoder.withIndent('  ').convert(userEnvironment));
  await packageRunCi('.');
}
