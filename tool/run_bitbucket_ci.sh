#!/usr/bin/env bash

set -xe

# Setup
dart pub get
dart run tekartik_bitbucket_ci_flutter:install --verbose
source $(dart run tekartik_bitbucket_ci_flutter:env)

# Run
dart tool/run_ci.dart
