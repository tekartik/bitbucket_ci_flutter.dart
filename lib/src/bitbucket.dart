import 'dart:io';

bool? _isEnvBitbucketCi;

bool get isEnvBitbucketCi =>
    _isEnvBitbucketCi ??
    (Platform.environment['BITBUCKET_BUILD_NUMBER'] != null);
