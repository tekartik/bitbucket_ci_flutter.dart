import 'dart:io';

import 'package:process_run/shell.dart';
import 'package:tekartik_bitbucket_ci_flutter/bitbucket_ci.dart';
import 'package:tekartik_travis_ci_flutter/bin/install.dart' as install;
import 'package:args/args.dart';

final _verboseFlag = 'verbose';

Future main(List<String> args) async {
  var parser = ArgParser()..addFlag(_verboseFlag, help: 'Verbose output');
  var result = parser.parse(args);

  var verbose = result[_verboseFlag] as bool;

  if (verbose) {
    stdout.writeln('isEnvBitbucketCi: $isEnvBitbucketCi');
    stdout.writeln(Platform.environment);
  }

  if (isEnvBitbucketCi) {
    var shell = Shell();
    // Install unzip needed by flutter
    await shell.run('''
apt-get update -qq
apt-get install -y -qq unzip
''');
  }
  await install.main();
}
