# bitbucket_ci_flutter

Bibucket pipelines helper scripts to allow running unit test on flutter

## Setup

This assumes you are familiar with Dart and Bitbucket integration

Include `tekartik_bitbucket_ci_flutter` as a development dependencies in your `pubspec.yaml` file

```
dev_dependencies:
  tekartik_bitbucket_ci_flutter:
    git:
      url: https://bitbucket.org/tekartik/bitbucket_ci_flutter.dart
      ref: dart3a
    version: '>=0.1.0'
```

Create the following `bitbucket-pipelines.yml` file

```yaml
image: google/dart
pipelines:
  default:
    - step:
        script:
          - tool/run_bitbucket_ci.sh
```

Create the following `tool/run_bitbucket_ci.sh` 

```bash
#!/usr/bin/env bash

set -xe

# Setup
pub get
dart run tekartik_bitbucket_ci_flutter:install --verbose
source $(dart run tekartik_bitbucket_ci_flutter:env)

# Run
dart tool/run_ci.dart
```

Create and customize `tool/run_ci.dart` to run your tests.

Commit/push and configure your project (on bitbucket website) to run pipelins

## Example

This project use the solution itself and simply execute `flutter doctor`